﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// create the Rotator class
// This will allow each of the pickups to rotate on an axis
public class Rotator : MonoBehaviour
{
 

    // Update is called once per frame
    void Update()
    {
        // rotate each individual pick up by these positions on the vector3 multiplied by deltaTime
        transform.Rotate(new Vector3(15, 30, 45) * Time.deltaTime);
    }
}
