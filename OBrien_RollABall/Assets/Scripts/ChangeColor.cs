﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// create the ChangeColor class
// this will allow the player to change color when a certain key is pressed
public class ChangeColor : MonoBehaviour
{

    // Update is called once per frame
    void Update()
    {
        // if the user clicks the R key
       if (Input.GetKeyDown(KeyCode.R))
        {
            // change the player color to Red
            GetComponent<Renderer>().material.color = Color.red;
        }
       // if the user clicks the G key
       if (Input.GetKeyDown(KeyCode.G))
        {
            // change the player color to Green
            GetComponent<Renderer>().material.color = Color.green;
        }
       // if the user clicks the B key
       if (Input.GetKeyDown(KeyCode.B))
        {
            // change the player color to Blue
            GetComponent<Renderer>().material.color = Color.blue;
        }
    }
}
