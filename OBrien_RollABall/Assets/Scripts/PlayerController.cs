﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


/* Create a public class for the player
 * This class will affect the ball itself (the player)
 */
public class PlayerController : MonoBehaviour
{
    // A float to generate speed for the player movement
    public float speed;

    // two public text variables to display text that can be acted upon 
    public Text countText;
    public Text winText;

    // boolean variable to test if the player is on the ground or not
    bool onGround = true;

    // float variable that will determine the player jumping ability (height)
    public float jumpPower;

    // create the rigid body for the player
    private Rigidbody rb;

    // create integer count to add player score
    private int count;

    // Start is called before the first frame update
    void Start()
    {
        // create the rigid body variable in the start class
        rb = GetComponent<Rigidbody>();
        // begin the count at zero
        count = 0;
        // bring the SetCountText function
        SetCountText();
        // print the text on the screen
        winText.text = "";
        
    }

    void Update()
    {
        // if the players y position is under 10
        if (transform.position.y < -10f)
        {
            // bring in the RestartLevel function
            RestartLevel();
        }

        // determine where in the level is considered the "ground"
        onGround = Physics.Raycast(transform.position, Vector3.down, .52f);

        // if the player is on the ground and the user presses the jump button
        if (Input.GetButtonDown("Jump") && onGround)
        {
            // intiate the jump function 
            Jump ();
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        // pressing the left and right keys moves the player in a horizontal direction
        float moveHorizontal = Input.GetAxis("Horizontal");

        // pressing the up and down keys moves the player in a vertical direction
        float moveVertical = Input.GetAxis("Vertical");

        // set the vector3 position to wherever the player moves the ball 
        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

        // move the player by using the movement vector multiplied by speed
        rb.AddForce(movement * speed);
    }

    // create the OnTriggerEnter function 
    void OnTriggerEnter(Collider other)
    {
        // if the object that is collided with is a PickUp
        if (other.gameObject.CompareTag("PickUp"))
        {
            // delete the object from the world
            other.gameObject.SetActive(false);
            // add one to the counter
            count = count + 1;
            // set the updated text with the new counter
            SetCountText();
        }
    }

    // create the SetCountText function 
    void SetCountText ()
    {
        // create the counter so the player can see their score
        countText.text = "Count: " + count.ToString();
        // if the count of the score reaches 17 or more
        if (count >= 17)
        {
            // print that the player wins
            winText.text = "You Win!";
        }
    }

    // create the RestartLevel function
    void RestartLevel ()
    {
        // call onto the scene manager to set the spawn point back to the middle of the level
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    // create the Jump fuction
    void Jump ()
    {
        // affect the rigidbody by multiplying its vector3 y position by jump power
        rb.AddForce(Vector3.up * jumpPower);
    }
}