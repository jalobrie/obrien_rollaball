﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// create the CameraController class
public class CameraController : MonoBehaviour
{
    // public variable game obect that connects to the player
    public GameObject player;

    // private variable thats a vector3 
    private Vector3 offset;


    // Start is called before the first frame update
    void Start()
    {
        // offset variable determines position of the camera
        offset = transform.position - player.transform.position;
    }

    // Update is called once per frame
    void LateUpdate ()
    {
        // camera will follow the player around as they move around the map
        transform.position = player.transform.position + offset;
    }
}
